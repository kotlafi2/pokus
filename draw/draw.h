#ifndef DRAWWINDOW_H
#define DRAWWINDOW_H

#include "precompiled.h"

QT_BEGIN_NAMESPACE
namespace Ui { class DrawWindow; }
QT_END_NAMESPACE

class DrawWindow : public QMainWindow
{
    Q_OBJECT

public:
    DrawWindow(QWidget *parent = nullptr);
    ~DrawWindow();

private:
    Ui::DrawWindow *ui;
};
#endif // DRAWWINDOW_H
