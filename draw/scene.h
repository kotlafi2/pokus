#ifndef SCENE_H
#define SCENE_H

#include "precompiled.h"

class Scene : public QGraphicsScene
{
public:
    Scene ();

protected:
    void dragEnterEvent (QGraphicsSceneDragDropEvent *event) override;
    void dragMoveEvent(QGraphicsSceneDragDropEvent *event) override;
    void dropEvent (QGraphicsSceneDragDropEvent *event) override;
};

#endif // SCENE_H
