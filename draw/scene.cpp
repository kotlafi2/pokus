#include "scene.h"

Scene::Scene()
{

}

void Scene::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
    const QMimeData * data = event->mimeData();
    event->setAccepted (data->hasColor());
}

void Scene::dragMoveEvent(QGraphicsSceneDragDropEvent *event)
{
}

void Scene::dropEvent (QGraphicsSceneDragDropEvent *event)
{
    const QMimeData * data = event->mimeData();
    if (data->hasColor())
    {
        QColor color = data->colorData().value <QColor> ();
        QPointF pos = event->scenePos ();
        QGraphicsItem * item = itemAt (pos, QTransform ());
        if (item == nullptr)
        {
            QBrush brush = backgroundBrush();
            brush.setColor (color);
            setBackgroundBrush (brush);
        }
        else if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
        {
            if (event->proposedAction() == Qt::CopyAction) // Ctrl Mouse
                shape->setPen (color);
            else
                shape->setBrush (color);
        }
        else if (auto line = dynamic_cast < QGraphicsLineItem * > (item))
        {
            QPen pen = line->pen ();
            pen.setColor (color);
            line->setPen (pen);
        }
    }
}


