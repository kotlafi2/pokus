#ifndef COLORBUTTON_H
#define COLORBUTTON_H

#include "precompiled.h"

class ColorButton : public QToolButton
{
public:
    ColorButton (QToolBar * parent, QString p_name ) ;
private:
    QString name;
    QColor color;
    QPixmap getPixmap ();

protected:
    void mousePressEvent (QMouseEvent * event) override;
};

#endif // COLORBUTTON_H
