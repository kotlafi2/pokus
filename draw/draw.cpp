#include "precompiled.h"
#include "draw.h"
#include "ui_draw.h"

#include "colorbutton.h"
#include "scene.h"

DrawWindow::DrawWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::DrawWindow)
{
    ui->setupUi(this);

    ui->vsplitter->setStretchFactor (0, 4);
    ui->vsplitter->setStretchFactor (1, 1);

    ui->splitter->setStretchFactor (0, 1);
    ui->splitter->setStretchFactor (1, 3);
    ui->splitter->setStretchFactor (2, 1);

    ui->palette->clear ();
    QToolBar * colorBar = new QToolBar (this);
    ui->palette->addTab (colorBar, "Colors");

    QStringList list;
    list << "red" << "green" << "blue" << "orange" << "yellow" << "lime";

    for (QString name : list)
        new ColorButton (colorBar, name);

    // #include "scene.h"
    Scene * scene = new Scene;
    ui->graphicsView->setScene(scene);

    const int n = 16;
    QBitmap texture (n, n);
    texture.clear ();

    QPainter painter (&texture);
    painter.drawLine (0, 0, n-1, 0);
    painter.drawLine (0, 0, 0, n-1);
    painter.end ();

    QBrush brush ("cornflowerblue");
    brush.setTexture (texture);
    scene->setBackgroundBrush (brush);

    scene->addLine (0, 0, 200, 100, QPen (QColor ("red"), 7) );

    QGraphicsRectItem * r = new QGraphicsRectItem;
    r->setPos (100, 100);
    r->setRect (0, 0, 200, 100);
    r->setPen (QColor ("blue"));
    r->setBrush (QColor ("yellow"));
    r->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
    scene->addItem (r);

    for (int i = 1; i <= 2; i++)
    {
        QGraphicsEllipseItem * e = new QGraphicsEllipseItem;
        e->setPos (40 + 80 *(i-1), 30);
        e->setRect (0, 0, 40, 40);
        e->setPen (QColor ("blue"));
        e->setBrush (QColor ("cornflowerblue"));
        e->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
        e->setParentItem (r);
    }

}

DrawWindow::~DrawWindow()
{
    delete ui;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    DrawWindow w;
    w.show();
    return a.exec();
}
